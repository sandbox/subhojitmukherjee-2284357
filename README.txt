CONTENT OF THIS FILES
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting

INTRODUCTION
------------
Some project needs the ability to send invitation from the Drupal site. This module gives the feature to send invitation to an user. Site admin can manage the configuration from the admin panel and user can send invitation from a block.

REQUIREMENTS
------------
This module requires the following modules:
	1. User Referral (https://drupal.org/project/referral)
	2. Ckeditor (https://drupal.org/project/ckeditor)
	
INSTALLATION
------------
Install and enable both the require module i.e. User Referral and Ckeditor after that install this module and enable it. This module enables the Drupal HTML email during installation. You have to enable the block named "Invite an User" to see the block.

CONFIGURATION
-------------
Configure the module by visiting Configuration >> People >> Invite an User
Configure the permission by visiting Administrator >> People >> Permissions >> "Invite an user" to provide the permission of the configuration page for selected user roles.

TROUBLESHOOTING
---------------
If you can not see the block then please clear your Drupal cache.